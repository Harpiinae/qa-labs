<?php
use PHPUnit\Framework\TestCase;
require_once 'MyClass.php';

class FakeExceptionAnalyzer implements iExceptionAnalyzer {
    private $criticalExceptions;
    public $boolCrit = false;

    public function FakeExceptionAnalyzer() {
        $this->criticalExceptions = json_decode('
            [
                "value less then zero",
                "value is not number"
            ]
        ');
    }

    public function isCritical(Exception $exception) {
        return $this->boolCrit;
    }
}

class FakeExceptionManager implements iExceptionManager {
    private $critCount = 0;
    private $notCritCount = 0;
    private $connCrit = 0;
    private $ex;
    private $conn;
    public $boolSend = false;

    public function FakeExceptionManager() {
        $this->ex = new FakeExceptionAnalyzer();
        $this->ex->boolCrit = true;
        $db = DB::getInstance();
        $this->conn = $db->getConnection();
    }

    public function critCount(Exception $exception) {
        if($this->ex->isCritical($exception)) {
            $this->critCount++;
        } else {
            $this->notCritCount++;
        }
        if(!$this->message($exception->getMessage())) {
            $this->connCrit++;
        }
    }

    public function message($msg) {
        return $this->boolSend;
    }

    public function exCritCount() {
        return $this->critCount;
    }

    public function exNotCritCount() {
        return $this->notCritCount;
    }

    public function exConnCrit() {
        return $this->connCrit;
    }
}

class MyClassTest extends TestCase {
    private $my;

    public function MyClassTest($name = null, array $data = [], $dataName = '') {
        $fkmn = new FakeExceptionManager();
        $fkmn->boolSend = true;
        $this->my = new MyClass($fkmn);
        parent::__construct($name, $data, $dataName);
    }

    public function testWithoutExceptions() {
        $this->my->mysqrt(9);
        $this->my->mysqrt(16);
        $this->my->mysqrt(25);

        $this->assertEquals($this->my->critNumb(),0);
        $this->assertEquals($this->my->notCritNumb(),0);
    }

    public function testWithCriticalException() {
        $this->my->mysqrt(-2);
        $this->my->mysqrt(-8);

        $this->assertEquals($this->my->critNumb(),2);
        $this->assertEquals($this->my->notCritNumb(),0);
    }

    public function testWithNotCriticalException() {
        $this->my->mysqrt(0);
        $this->my->mysqrt(-4);
        $this->my->mysqrt('2a');
        $this->my->mysqrt(1);

        $this->assertEquals($this->my->critNumb(),3);
        $this->assertEquals($this->my->notCritNumb(),0);
    }
}