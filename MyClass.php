<?php

require_once 'model/DB.php';

interface iExceptionAnalyzer
{
    public function isCritical(Exception $exception);
}

interface iExceptionManager
{
    public function critCount(Exception $exception);
    public function message($msg);
    public function exCritCount();
    public function exNotCritCount();
    public function exConnCrit();
}

class ExceptionAnalyzer implements iExceptionAnalyzer {
    private $criticalExceptions;

    public function ExceptionAnalyzer() {
        $this->criticalExceptions = json_decode(file_get_contents("critical.err"));
    }

    public function isCritical(Exception $exception) {
        foreach ($this->criticalExceptions as $e) {
            if($e == $exception->getMessage()) {
                return true;
            }
        }
        return false;
    }
}

class ExceptionManager implements iExceptionManager {
    private $critCount = 0;
    private $notCritCount = 0;
    private $connCrit = 0;
    private $ex;
    private $conn;

    public function ExceptionManager() {
        $this->ex = new ExceptionAnalyzer();
        $db = DB::getInstance();
        $this->conn = $db->getConnection();
    }

    public function critCount(Exception $exception) {
        if($this->ex->isCritical($exception)) {
            $this->critCount++;
        } else {
            $this->notCritCount++;
        }
        if(!$this->message($exception->getMessage())) {
            $this->connCrit++;
        }
    }

    public function message($msg) {
        $stmt = $this->conn->prepare("
            INSERT INTO log(msg) VALUES (?);
        ");
        $stmt->bindParam(1,$msg);
        return $stmt->execute();
    }

    public function exCritCount() {
        return $this->critCount;
    }

    public function exNotCritCount() {
        return $this->notCritCount;
    }

    public function exConnCrit() {
        return $this->connCrit;
    }
}

class MyClass {
    private $exM;

    public function MyClass(iExceptionManager $mng) {
//        $this->exM = new exceptionManager();
        $this->exM = $mng;
    }

    public function mysqrt($val) {
        try {
            if($val == 0) {
                throw new Exception("sqrt of zero");
            }
            if($val < 0) {
                throw new Exception("value less then zero");
            }
            if (!is_numeric($val)) {
                throw new Exception("value is not number");
            }
        } catch (Exception $e) {
            $this->exM->critCount($e);
        }
    }

    public function critNumb() {
        return $this->exM->exCritCount();
    }
    public function notCritNumb() {
        return $this->exM->exNotCritCount();
    }
    public function connNumb() {
        return $this->exM->exConnCrit();
    }
}